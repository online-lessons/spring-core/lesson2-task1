package org.example.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@ComponentScan(basePackages = "org.example")
@Configuration
@PropertySource(value = "classpath:city.properties")
public class ApplicationConfig {

}

