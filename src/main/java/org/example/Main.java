package org.example;

import lombok.extern.java.Log;
import org.example.config.ApplicationConfig;
import org.example.model.City;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@Log
public class Main {
    public static void main(String[] args) {
        log.info("Hello world!");

        try (var context = new AnnotationConfigApplicationContext(ApplicationConfig.class)){
            City city1 = context.getBean(City.class);
            City city2 = context.getBean(City.class);

            log.info(city1.toString());
            log.info(city2.toString());

            log.info(city1.hashCode() + "");
            log.info(city2.hashCode() + "");

            log.info(context.getBean(ApplicationConfig.class).toString());
        }
    }
}