package org.example.model;

import lombok.Getter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Getter
@ToString
@Component
@Scope(scopeName = "prototype")
public class City {
    @Value("${city.name}")
    String name;

    @Value("${city.numberOfPeople}")
    int numberOfPeople;

    @Value("${city.area}")
    String area;
}
